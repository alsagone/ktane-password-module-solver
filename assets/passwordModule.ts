export const words: string[] = [
  'about',
  'after',
  'again',
  'below',
  'could',
  'every',
  'first',
  'found',
  'great',
  'house',
  'large',
  'learn',
  'never',
  'other',
  'place',
  'plant',
  'point',
  'right',
  'small',
  'sound',
  'spell',
  'still',
  'study',
  'their',
  'there',
  'these',
  'thing',
  'think',
  'three',
  'water',
  'where',
  'which',
  'world',
  'would',
  'write',
]

const clearUserInput = (str: string): string => {
  return Array.from(new Set(str.split(',')))
    .toString()
    .replace(/[^a-z]/gi, '')
    .toUpperCase()
}
export const buildRegExp = (possibilities: string[]): RegExp => {
  let pattern: string = ''

  for (const p of possibilities.map(clearUserInput)) {
    pattern += p ? `[${p}]` : '[A-Z]'
  }

  return new RegExp(`^${pattern}$`, 'gi')
}

export const filterWords = (regex: RegExp | null): string[] => {
  return regex ? words.filter((w) => regex.test(w)) : words
}
